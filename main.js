const { app, BrowserWindow, BrowserView, ipcMain } = require('electron')
const path = require('path')
let currentToLoad;
const urls = { 
	webconf: 'https://webconf.numerique.gouv.fr',
	webinaire: 'https://webinaire.numerique.gouv.fr',
};

const createWindow = (toLoad, url) => {
	const win = new BrowserWindow({
		width: 1400, 
		height: 1500,
		webPreferences: {
			preload: path.join(__dirname, 'preload.js'),
			nodeIntegration: true,
		},
	})

	if( toLoad && urls[toLoad] ){
		if( toLoad === 'webinaire' && url !== '' ){
			win.loadURL(url);
		}else{
			win.loadURL(urls[toLoad]);
		}
	}else{
		win.loadFile('index.html')
	}
}

app.whenReady().then(() => {
	createWindow()
	ipcMain.on('webinaireLoad', (event, url) => {
		currentToLoad = 'webinaire';
		createWindow('webinaire', url)
	})
	ipcMain.on('webconfLoad', (event) => {
		currentToLoad = 'webconf';
		createWindow('webconf')
	})

	app.on('activate', () => {
		if (BrowserWindow.getAllWindows().length === 0){
			createWindow(currentToLoad)
		}
	})
})

app.on('window-all-closed', () => {
	if ( process.platform !== 'darwin' ){
		app.quit()
	}
})

