onload = () => {
	let webinaireBtn = document.querySelector('#webinaire');
	let webconfBtn = document.querySelector('#webconf');
	let webinaireURL = document.querySelector('#webinaireURL');
	webinaireBtn.addEventListener('click', (e) => {
		window.IPC_API.webinaireLoad(webinaireURL.value);
	});
	webconfBtn.addEventListener('click', (e) => {
		window.IPC_API.webconfLoad();
	});
}
