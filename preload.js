const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('IPC_API', { 
	desktop: true, 
	test: true,
	webinaireLoad: (message) => {
		ipcRenderer.send('webinaireLoad', message)
	},
	webconfLoad: (message) => {
		ipcRenderer.send('webconfLoad', message)
	}
})
